﻿using System;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Nancy;
using Nancy.Hosting.Self;
using PetaPoco;

namespace DbChart
{
    [TableName("entities")]
    [PrimaryKey("id")]
    class Entity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Size { get; set; }
    }

    static class Db
    {
        private static SQLiteConnection _sqLiteConnection;
        private const string DatabaseFileName = "db.sqlite";

        private static Database _instance;
        public static Database Instance
        {
            get
            {
                if (_instance == null)
                {
                    _sqLiteConnection =
                        new SQLiteConnection(string.Format("Data Source={0};Version=3;", DatabaseFileName));
                    _sqLiteConnection.Open();
                    SQLiteConnection con = _sqLiteConnection;
                    _instance = new Database(con);
                }
                return _instance;
            }
        }

        public static void CreateDatabateIfNeeded()
        {

            if (!File.Exists(DatabaseFileName))
            {
                SQLiteConnection.CreateFile(DatabaseFileName);

                {
                    string sql = "CREATE TABLE entities (id INTEGER PRIMARY KEY,name VARCHAR(20), size INT)";
                    Instance.Execute(sql);
                    var e = new Entity { Size = 42, Name = "asd" };
                    Instance.Insert(e);
                    e = new Entity { Size = 54, Name = "uuu" };
                    Instance.Insert(e);
                }
            }
        }
    }

    public class PathProvider : IRootPathProvider
    {
        public string GetRootPath()
        {
            return Path.GetFullPath(Path.Combine(Environment.CurrentDirectory));//, @"..\"));
        }
    }
    public class CustomBootstrapper : DefaultNancyBootstrapper
    {
        protected override IRootPathProvider RootPathProvider
        {
            get { return new PathProvider(); }
        }
    }
    public class MainModule : NancyModule
    {
        public MainModule()
        {
//            const string tmpl = @"<script type=""text/javascript"" src=""https://www.google.com/jsapi""></script>
//  <div id=""chart_div""><div>";
//            Get["/"] =
//                _ => tmpl + String.Join("", Db.Instance.Query<Entity>("").Select(e => String.Format("<li>{0}</li>", e.Name)));
            string js = @"google.load('visualization', '1', {packages: ['corechart', 'bar']});
google.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = google.visualization.arrayToDataTable([
            ['Name','Size'],
          " +String.Join(",\n", Db.Instance.Query<Entity>("").Select(e => String.Format("['{0}',{1}]", e.Name, e.Size)))+@"
          ]);

      var options = {
        title: 'Population of Largest U.S. Cities',
        chartArea: {width: '50%'},
        hAxis: {
          title: 'Total Population',
          minValue: 0
        },
        vAxis: {
          title: 'City'
        }
      };

      var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

      chart.draw(data, options);
    }";
            Get["/"] = _ => View["chart", new{Script=js}];
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Db.CreateDatabateIfNeeded();

            var ents = Db.Instance.Query<Entity>("").ToList();
            foreach (var entity in ents)
            {
                Console.WriteLine("{0} {1} {2}", entity.Id, entity.Name, entity.Size);
            }

            var httpLocalhost = "http://localhost:1234";
            using (var host = new NancyHost(new Uri(httpLocalhost)))
            {
                host.Start();
                Process.Start(httpLocalhost);
                Console.ReadLine();
            }
        }
    }
}
